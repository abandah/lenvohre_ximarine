﻿using Acr.UserDialogs;
using MobApp_LenvoHRE.Files.constant;
using MobApp_LenvoHRE.Files.Refit;
using MobApp_LenvoHRE.Files.Views;
using NavigationDrawer.MenuItems;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using QRCodeScanner.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace MobApp_LenvoHRE.Files.SignInViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Manual_Login : ContentPage, OnResponse<MasterPageItem>
	{
		public Manual_Login()
		{
			InitializeComponent();
			App.Current.On<Xamarin.Forms.PlatformConfiguration.Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
			entryLink.Text = Global.Link;
		}
		private void namenextclicked(object sender, EventArgs e)
		{

			if (string.IsNullOrWhiteSpace(entryUserName.Text))
			{
				DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), Helpers.TranslateExtension.Translate("Name_is_required"), Helpers.TranslateExtension.Translate("Ok"));
			}else if (string.IsNullOrWhiteSpace(entryPassword.Text))
			{
				DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), Helpers.TranslateExtension.Translate("Password_is_required"), Helpers.TranslateExtension.Translate("Ok"));
			}else if (string.IsNullOrWhiteSpace(entryLink.Text))
			{
				DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), Helpers.TranslateExtension.Translate("QRCode_is_required"), Helpers.TranslateExtension.Translate("Ok"));
			}else
			{
				String username = entryUserName.Text.ToString();
				String Password = entryPassword.Text.ToString();
				String Link = entryLink.Text.ToString().ToLower();
				if (!Link.EndsWith("/"))
				{
					Link = Link + "/";
				}
				Global.Link = Link;
				Global.setDeviceSerial(username);
				GetLenvoToken(username, Password);
			}
		}
		void OnImageqr(object sender, EventArgs args)
		{
			if (Util.Validation.IsConnected())
			{
				try
				{
					btnScan_Clicked();
				}
				catch (Exception ex)
				{
					throw ex;
				}
			}
			else
			{
				shadaAsync();
			}	
		}
		private async void btnScan_Clicked()
		{

			try
			{
				var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
				if (status != PermissionStatus.Granted)
				{
					if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
					{
						await DisplayAlert(Helpers.TranslateExtension.Translate("Need_Camera"), Helpers.TranslateExtension.Translate("LenvoHRE_needs_camera_access"), Helpers.TranslateExtension.Translate("OK"));
					}

					var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
					//Best practice to always check that the key exists
					if (results.ContainsKey(Permission.Camera))
						status = results[Permission.Camera];
				}

				if (status == PermissionStatus.Granted)
				{
					var ScannerPage = new ZXingScannerPage() { Title = Helpers.TranslateExtension.Translate("Scan_QR_code") };

					ScannerPage.OnScanResult += (result) =>
					{
						ScannerPage.IsScanning = false;

						Device.BeginInvokeOnMainThread(() =>
						{
							Navigation.PopAsync();
							if (result != null)
							{
								String[] parameter = result.Text.Split(',');
								if (!parameter[0].Equals("LenvoHRE"))
									return;
								if (parameter.Length == 2)
									havingLink(parameter[1]);
								if (parameter.Length == 3)
									havingLink_Token(parameter[1], parameter[2]);
							}
						});
					};

					await Navigation.PushAsync(ScannerPage);
				}
				else if (status != PermissionStatus.Unknown)
				{
					await DisplayAlert(Helpers.TranslateExtension.Translate("Camera_Denied"), Helpers.TranslateExtension.Translate("Can_not_continue"), Helpers.TranslateExtension.Translate("OK"));
				}
			}
			catch (Exception ex)
			{
				ex.ToString();
			}
			//contentpage.Content = ScannerPage();

			//try
			//{
			//	var scanner = DependencyService.Get<IQrScanningService>();
			//	var result = await scanner.ScanAsync();
			//	if (result != null)
			//	{
			//		String[] parameter = result.Split(',');
			//		if (!parameter[0].Equals("LenvoHRE"))
			//			return;
			//		if (parameter.Length == 2)
			//			havingLink(parameter[1]);
			//		if (parameter.Length == 3)
			//			havingLink_Token(parameter[1], parameter[2]);

			//	}
			//}
			//catch (Exception ex)
			//{

			//	throw;
			//}
		}
		private void GetLenvoToken(string username, string password)
		{
			Dictionary<string, string> data = new Dictionary<string, string> {
					{WSConstants.PARAM_USERNAME,username },
					{WSConstants.PARAM_PASSWORD,password },
					{WSConstants.PARAM_DEVIC_SERIAL,username }
				};
			Global.setDeviceSerial(username);

			CallApi_GetLenvoToken(data);
		}

		private void CallApi_GetLenvoToken(Dictionary<string, string> data)
		{
			CallApi.HandelErrors<MasterPageItem>(Fun_GetLenvoToken, data, WSConstants.GET_LENVO_TOKEN, this);
		}

		private async Task<WSResponse<MasterPageItem>> Fun_GetLenvoToken(Dictionary<string, string> arg)
		{


			String l = Global.Link + constant.WSConstants.API_URL;
			LenvoJobsAPIs<MasterPageItem> registerAPI = CallApi.Caller<LenvoJobsAPIs<MasterPageItem>>(Global.Link + constant.WSConstants.API_URL);

			WSResponse<MasterPageItem> wSResponse = await registerAPI.LoginDeviceUser(arg);

			return wSResponse;
		}

		void OnResponse<MasterPageItem>.OnResult(int CallId, WSResponse<MasterPageItem> response)
		{

			switch (CallId)
			{
				case WSConstants.GET_LENVO_TOKEN:
					OnResult_GetLenvoToken(CallId, response);
					break;

				case WSConstants.REGISTER_DEVICE:
					OnResult_CallApi_RegisterDevice(CallId, response);
					break;

				case WSConstants.NoInterNet:
					shdaAsync();
					break;
			}

		}

		private async void shdaAsync()
		{
			var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
			{
				Message = Helpers.TranslateExtension.Translate("There_is_no_internet_connection"),
				OkText = Helpers.TranslateExtension.Translate("Retry"),
				CancelText = Helpers.TranslateExtension.Translate("Close")
			});
			if (result)
			{

				namenextclicked(null,null);
			}
			else
			{
				//App.Current.MainPage = new Exit();
			}
		}

		private async void shadaAsync()
		{
			var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
			{
				Message = Helpers.TranslateExtension.Translate("There_is_no_internet_connection"),
				OkText = Helpers.TranslateExtension.Translate("Retry"),
				CancelText = Helpers.TranslateExtension.Translate("Close")
			});
			if (result)
			{

				OnImageqr(null, null);
			}
			else
			{
				//App.Current.MainPage = new Exit();
			}
		}

		private void OnResult_GetLenvoToken(int callId, WSResponse<MasterPageItem> response)
		{
			if (response.Status == 1)
			{
				Global.setDeviceSerial<string>(response.LenvoLoginToken);
				Dictionary<string, string> data = new Dictionary<string, string> {
					{WSConstants.PARAM_DEVIC_SERIAL,response.LenvoLoginToken },
					{WSConstants.PARAM_LENVO_TOKEN, response.LenvoLoginToken},
					 { WSConstants.PARAM_GCM_TOKEN,Global.getGCM()},
					 { WSConstants.PARAM_DEVICE_TYPE,constant.WSConstants.OS_TYPE}
				};

				CallApi_RegisterDevice(data);
			}
			else
			{
				DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), response.getMessage(), Helpers.TranslateExtension.Translate("Ok"));
			}//else if (response.Status == 0)
			//{
			//	DisplayAlert("Failed", "Check your information", "Ok");
			//}
			//else if (response.Status == -1)
			//{
			//	DisplayAlert("Failed", "Check your URL", "Ok");
			//}
		}
		private void CallApi_RegisterDevice(Dictionary<string, string> data)
		{
			CallApi.HandelErrors<MasterPageItem>(RegisterDevice, data, WSConstants.REGISTER_DEVICE, this);

		}
		private async Task<WSResponse<MasterPageItem>> RegisterDevice(Dictionary<string, string> data)
		{

			LenvoJobsAPIs<MasterPageItem> registerAPI = CallApi.Caller<LenvoJobsAPIs<MasterPageItem>>(Global.Link + constant.WSConstants.API_URL);
			return await registerAPI.RegisterDevice(data);
		}
		private void OnResult_CallApi_RegisterDevice(int callId, WSResponse<MasterPageItem> response)
		{
			if (response.Status == 1)
			{
				SavePrefAndGlobizeAsync(response.UserId, response.LenvoLoginToken);
			}
			else
			{
				Util.ProgressDialog.HideProgressDialog();
				DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), response.Message, Helpers.TranslateExtension.Translate("OK"));

			}
		}
		private async Task SavePrefAndGlobizeAsync(string userId, string lenvoLoginToken)
		{
			await Pref.SaveApplicationProperty(Pref.USER_ID, userId);
			await Pref.SaveApplicationProperty(Pref.LENVO_LOGIN_TOKEN, lenvoLoginToken);
			await Pref.SaveApplicationProperty(Pref.IS_REMEMBER_ME, true);
			await Pref.SaveApplicationProperty(Pref.LINK, Global.Link);
			Util.ProgressDialog.HideProgressDialog();

			App.Current.MainPage = new SplashPage(null);


		}

		private async void havingLink(string link)
		{
			await Pref.SaveApplicationProperty(Pref.LINK, link);
			Global.getLink();
			entryLink.Text = Global.Link;
		}

		private void havingLink_Token(String link, String token)
		{
			Global.Link = link;
			Dictionary<string, string> data = new Dictionary<string, string> {
					{WSConstants.PARAM_DEVIC_SERIAL,token},
					{WSConstants.PARAM_LENVO_TOKEN,token},
					 { WSConstants.PARAM_GCM_TOKEN,Global.getGCM()},
					 { WSConstants.PARAM_DEVICE_TYPE,constant.WSConstants.OS_TYPE}
				};
			Global.setDeviceSerial(token);

			CallApi_RegisterDevice(data);
		}
	}
}