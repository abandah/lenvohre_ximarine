﻿using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MobApp_LenvoHRE.Files.Service
{
	class LocationGetter
	{
		private static Xamarin.Forms.GoogleMaps.Position position;
		
		public static bool IsLocationEmpty()
		{
			return (position.Latitude==0)||(position.Longitude==0);
		}

		public static async Task<Xamarin.Forms.GoogleMaps.Position> GetNewLocation()
		{
			var locator = CrossGeolocator.Current;
			//CancellationTokenSource ctsrc = new CancellationTokenSource(2000);
			//var location = await locator.GetPositionAsync(TimeSpan.FromSeconds(2), ctsrc.Token);
			var location = await locator.GetPositionAsync();

			position = new Xamarin.Forms.GoogleMaps.Position(location.Latitude, location.Longitude);
			return position;
		}

		public static async Task<Xamarin.Forms.GoogleMaps.Position> getlocation()
		{
			if (IsLocationEmpty())
			{
				return await GetNewLocation();
				
			}
			else
			{
				return position;
			}
		}
	}
}
