﻿using Acr.UserDialogs;
using MobApp_LenvoHRE.Files.constant;
using MobApp_LenvoHRE.Files.Constant;
using MobApp_LenvoHRE.Files.HybridWebViews;
using MobApp_LenvoHRE.Files.Models;
using MobApp_LenvoHRE.Files.Refit;
using MobApp_LenvoHRE.Files.Util;
using MobApp_LenvoHRE.Files.View;
using NavigationDrawer.MenuItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static NavigationDrawer.MenuItems.MasterPageItem;

namespace MobApp_LenvoHRE.Files.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : MasterDetailPage, OnResponse<MasterPageItem>
	{

		private string data;

		public MainPage(string data)
		{

			InitializeComponent();

			this.data = data;
			NavigationPage.SetHasNavigationBar(this, false);
			this.IsPresentedChanged+= OnPresentedChanged;

			if (data != null || data != string.Empty)
			{
				Detail = new NavigationPage(new Profile(data))
				{
					BarBackgroundColor = Color.FromHex("#6BBFED"),
					BarTextColor = Color.FromHex("#FFFFFF"),
					Icon = ""
				};
			}
			else
			{
				Detail = new NavigationPage(new Profile(Global.getLink()))
				{
					BarBackgroundColor = Color.FromHex("#6BBFED"),
					BarTextColor = Color.FromHex("#FFFFFF"),
					Icon = ""
				};
			}

			CallApi_GetMenuItems();

		}

		private void OnPresentedChanged(object sender, EventArgs e) {
			if (this.IsPresented)
			{
				CallApi_GetMenuItems();
			}
			else
			{
				//CallApi_GetMenuItems();
			}
		}

		public void fillMenu(List<MasterPageItem> menuList)
		{
			menuList.Add(new MasterPageItem() { Title = Helpers.TranslateExtension.Translate("Settings"), Link = "Settings", IsClickable = true });
			menuList.Add(new MasterPageItem() { Title = Helpers.TranslateExtension.Translate("Exit"), Link = "Exit", IsClickable = true });

			StackLayout slmenu = new StackLayout();
			slmenu.HeightRequest = 28;
			slmenu.BackgroundColor = Color.FromHex("#FFFFFF");

			Grid menugrid = new Grid()
			{
				RowDefinitions =
					{
						new RowDefinition { Height = new GridLength(28, GridUnitType.Absolute) }
					},
				ColumnDefinitions =
					{
						new ColumnDefinition { Width = new GridLength(57, GridUnitType.Star) },
						new ColumnDefinition { Width = new GridLength(57, GridUnitType.Star) },
						new ColumnDefinition { Width = new GridLength(57, GridUnitType.Star) }
					}
			};
			//----------------------------------------------------------------------------------------
			var tapGestureRecognizer = new TapGestureRecognizer();

			Image img1;
			menugrid.Children.Add(img1 = new Image { Source = "dasboard.png", Margin = new Thickness(-3, 0, -3, -10), BackgroundColor = Color.FromHex("#5cb85c") }, 0, 0);
			img1.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(() =>
				{

					MasterPageItem it = new MasterPageItem();
					it.Link = Global.Link + "index.aspx";
					OnMenuItemSelected(it);


				})
			});

			Image img2;
			menugrid.Children.Add(img2 = new Image { Source = "user.png", Margin = new Thickness(-3, 0, -3, -10), BackgroundColor = Color.FromHex("#4f99c6") }, 1, 0);
			img2.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(() =>
				{

					MasterPageItem it = new MasterPageItem();
					it.Link = Global.Link + "DetailedInfo.aspx";
					OnMenuItemSelected(it);


				})
			});

			//Image img3;
			//menugrid.Children.Add(img3 = new Image { Source = "tablet.png", Margin = new Thickness(-3, 0, -3, -10), BackgroundColor = Color.FromHex("#e59729")}, 2, 0);
			//img3.GestureRecognizers.Add(new TapGestureRecognizer
			//{
			//	Command = new Command(() =>
			//	{

			//		MasterPageItem it = new MasterPageItem();
			//		it.Link = Global.Link + "QRBarcode.aspx";
			//		OnMenuItemSelected(it);


			//	})
			//});

			Image img4;
			menugrid.Children.Add(img4 = new Image { Source = "sitemap.png", Margin = new Thickness(-3, 0, -3, -10), BackgroundColor = Color.FromHex("#b74635") }, 2, 0);
			img4.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(() =>
				{

					MasterPageItem it = new MasterPageItem();
					it.Link = Global.Link + "OrganizaionalStructure/Organizaional_Structure.aspx";
					OnMenuItemSelected(it);


				})
			});
			//-----------------------------------------------------------------------------------------
			StackLayout sl = new StackLayout();
			ScrollView sv = new ScrollView();

			sl.Orientation = StackOrientation.Vertical;
			for (int i = 0; i < menuList.Count; i++)
			{
				MasterPageItem it = menuList[i];

				StackLayout con = new StackLayout();
				con.VerticalOptions = LayoutOptions.Fill;
				con.Orientation = StackOrientation.Horizontal;
				con.Padding = new Thickness(7, 7, 7, 5);
				con.Spacing = 6;

				Image img = new Image();
				//img.Source = it.Icon;
				img.VerticalOptions = LayoutOptions.Center;

				Label lab = new Label();
				lab.FontSize = 15;// Device.GetNamedSize(NamedSize.Small);
				lab.VerticalOptions = LayoutOptions.FillAndExpand;
				lab.FlowDirection = Device.FlowDirection;
				lab.HorizontalOptions = LayoutOptions.FillAndExpand;
				lab.TextColor = Color.Black;
				//lab.HorizontalTextAlignment = TextAlignment.Center;
				lab.Text = it.Title;

				Label lab2 = new Label();
				lab2.FontSize = 15;// Device.GetNamedSize(NamedSize.Small);
				lab2.VerticalOptions = LayoutOptions.FillAndExpand;
				lab2.HorizontalOptions = LayoutOptions.FillAndExpand;
				lab2.FlowDirection = Device.FlowDirection;
				lab2.TextColor = Color.FromHex("#6BBFED");
				lab2.Text = it.Title;

				if (it.IsClickable)
				{
					con.GestureRecognizers.Add(
						new TapGestureRecognizer()
						{
							Command = new Command(() =>
							{
								var buttons = sl.Children;
								foreach (StackLayout bt in buttons)
								{

									bt.BackgroundColor = Color.White;
								}
								con.BackgroundColor = Color.FromHex("#6BBFED");
								OnMenuItemSelected(it);

							})
						});
				}

				if (it.IsClickable)
				{
					con.Children.Add(img);
					con.Children.Add(lab);
				}
				else
				{
					con.Children.Add(lab2);
				}
				slmenu.Children.Add(menugrid);
				sl.Children.Add(slmenu);
				sl.Children.Add(con);
			}
			sv.Content = sl;

			myContentPage.Content = sv;
		}
		//private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
		//{
		//	var item = (MasterPageItem)e.SelectedItem;
		//	ContentPage page = item.GetTargetPage();
		//	//if(item.Link.Contains("UserNewRequest.aspx"))
		//	Detail = new NavigationPage(page)
		//	{
		//		BarBackgroundColor = Color.FromHex("#6BBFED"),
		//		BarTextColor = Color.FromHex("#FFFFFF")
		//	};
		//	IsPresented = false;
		//}
		private void OnMenuItemSelected(MasterPageItem master)
		{
			var item = master;
			ContentPage page = item.GetTargetPage();
			//if(item.Link.Contains("UserNewRequest.aspx"))
			Detail = new NavigationPage(page)
			{
				BarBackgroundColor = Color.FromHex("#6BBFED"),
				BarTextColor = Color.FromHex("#FFFFFF")
			};
			IsPresented = false;
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			//CallApi_GetMenuItems();

		}


		private void CallApi_GetMenuItems()
		{

			Dictionary<string, string> data = new Dictionary<string, string> {
					{WSConstants.PARAM_USERID, Global.UserId },
				{ WSConstants.PARAM_LENVO_TOKEN, Global.getLenvoLoginToken() },};

			CallApi.HandelErrors(GetMenuItems, data, WSConstants.CALL_MENU, this,false);


		}
		private async Task<WSResponse<MasterPageItem>> GetMenuItems(Dictionary<string, string> data)
		{

			LenvoJobsAPIs<MasterPageItem> GetMenuItems = CallApi.Caller<LenvoJobsAPIs<MasterPageItem>>(Global.Link + WSConstants.API_URL);
			//   registerAPI.Registration(data).Wait();
			WSResponse<MasterPageItem> wSResponse = await GetMenuItems.GetMenuItems(data);
			//  Debug.WriteLine(wSResponse.getMessage());
			return wSResponse;
		}

		void OnResponse<MasterPageItem>.OnResult(int CallId, WSResponse<MasterPageItem> response)
		{

			switch (CallId)
			{
				case WSConstants.CALL_MENU:
					OnResult_CallApi_GetMenuItems(CallId, response);
					break;

				case WSConstants.NoInterNet:
					//shdaAsync();
					break;
			}

		}

		//private async void shdaAsync()
		//{
		//	var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
		//	{
		//		Message = "There is no internet connection",
		//		OkText = "Retry",
		//		CancelText = "Close"
		//	});
		//	if (result)
		//	{
		//		//App.Current.MainPage = new MainPage();
		//		CallApi_GetMenuItems();
		//	}
		//	else
		//	{
		//		App.Current.MainPage = new Exit();
		//	}
		//}

		private void OnResult_CallApi_GetMenuItems(int callId, WSResponse<MasterPageItem> response)
		{
			if (response.Status == 1)
			{

				fillMenu(response.dataFromServer.DataRows);
				//SavePrefAndGlobizeAsync(response.LenvoLoginToken);
				//DisplayAlert("Success", "Success", "OK!");
			}
			else if (response.Status == 0)
			{
				DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), response.Message, Helpers.TranslateExtension.Translate("OK"));
			}
			else
			{
				dialogAPIAsync(response.Message);

			}


		}
		private async void dialogAPIAsync(string message)
		{
			var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
			{
				Message = message,
				OkText = Helpers.TranslateExtension.Translate("Retry"),
				CancelText = Helpers.TranslateExtension.Translate("Logout")
			});
			if (result)
			{
				CallApi_GetMenuItems();
			}
			else
			{
				Pref.CleanPref();
				App.Current.MainPage = new SplashPage(null);
			}
		}


	}
}
