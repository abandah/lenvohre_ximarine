﻿using Acr.UserDialogs;
using MobApp_LenvoHRE.Files.constant;
using MobApp_LenvoHRE.Files.Service;
using MobApp_LenvoHRE.Files.SignInViews;
using MobApp_LenvoHRE.Files.Util;
using Plugin.Geolocator;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobApp_LenvoHRE.Files.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashPage : ContentPage
	{
		private string data;

		public SplashPage(string data)
		{
			InitializeComponent();
			//LocationGetter.getlocation();
			this.data = data;
			if (Device.OS == TargetPlatform.iOS)
			{
				constant.WSConstants.OS_TYPE = "2";
				Start(PermissionStatus.Granted);
			}
			else
			{
				constant.WSConstants.OS_TYPE = "1";
			}
			Global.getDeviceSerial();
			StartTimer(Global.IsAllGloblized());
		}

		public void StartTimer(bool IsAllGloblized)
		{
			Device.StartTimer(TimeSpan.FromSeconds(5), () =>
			{
				if (IsAllGloblized == true)
				{
					StartLaunchPage();
				}
				else
				{
					StartLogIn();
				}
				return false; // True = Repeat again, False = Stop the timer
			});
		}
		public void StartLaunchPage()
		{
            ResetNavigationStack();
			if (Device.Idiom == TargetIdiom.Tablet)
			{
				ResetNavigationStack();
				App.Current.MainPage = new NavigationPage(new Views.ipad_page())
				{
					BarBackgroundColor = Color.FromHex("#6BBFED"),
					BarTextColor = Color.FromHex("#FFFFFF"),
					Icon = ""
				};
				
			}
			else
			{
				ResetNavigationStack();
				App.Current.MainPage = new NavigationPage(new Views.MainPage(data))
				{
					BarBackgroundColor = Color.FromHex("#6BBFED"),
					BarTextColor = Color.FromHex("#FFFFFF"),
					Icon = ""
				};
			}
				
		}
		public void StartLogIn()
		{
			ResetNavigationStack();
			App.Current.MainPage = new NavigationPage(new SignInMainPage())
			{
				BarBackgroundColor = Color.FromHex("#6BBFED"),
				BarTextColor = Color.FromHex("#FFFFFF"),
				Icon = ""
			};
		}
		//public async void StartPage(bool isGlobalized)
		//{
		//	if (isGlobalized)
		//	{
		//		StartTimer(new Views.MainPage());
		//	}
		//	else
		//	{
		//		StartTimer(new SignInMainPage());
		//	}
		//}

		public void ResetNavigationStack()
		{
			var existingPages = Navigation.NavigationStack.ToList();
			foreach (var page in existingPages)
			{
				Navigation.RemovePage(page);
			}
		}

		private async void Start(PermissionStatus status)
		{

			if (status == PermissionStatus.Granted)
			{
				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = WSConstants.LOCATION_ACCURACY;


				if (locator.IsGeolocationEnabled == false)
				{
					await DisplayAlert(Helpers.TranslateExtension.Translate("Need_location"), Helpers.TranslateExtension.Translate("LenvoHRE_needs_to_access_your_location_in_order_to_place_a_punch_in_your_attendance_log"), Helpers.TranslateExtension.Translate("OK"));
					//	await Navigation.PushAsync(new NeedLocation());
					return;
				}

				LocationGetter.getlocation();
				//StartCall<EntryTypes>();

			}
			else if (status != PermissionStatus.Unknown)
			{
				await DisplayAlert(Helpers.TranslateExtension.Translate("Location_Denied"), Helpers.TranslateExtension.Translate("Can_not_continue"), Helpers.TranslateExtension.Translate("OK"));
				return;
			}


		}

	}
}