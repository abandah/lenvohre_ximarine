﻿using Acr.UserDialogs;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Push;
using MobApp_LenvoHRE.Files.SignInViews;
using MobApp_LenvoHRE.Files.Views;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MobApp_LenvoHRE
{
	public partial class App : Xamarin.Forms.Application
	{
		//yyyjuhfg
		public static string UserName { get; set; }
		public static ImageSource ImageUser { get; set; }
		public static string Occupation { get; private set; }
		public static Page Detail { get; internal set; }
		private string data;

		public App()
		{
			InitializeComponent();
			MainPage = new SplashPage(null);
		}

		public App(string data)
		{
			InitializeComponent();

			this.data = data;
			Xamarin.Forms.PlatformConfiguration.AndroidSpecific.Application.SetWindowSoftInputModeAdjust(this, Xamarin.Forms.PlatformConfiguration.AndroidSpecific.WindowSoftInputModeAdjust.Resize);

			MainPage = new SplashPage(data);
		}

		protected override void OnStart()
		{
			// Handle when your app sleeps
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
